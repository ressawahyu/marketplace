<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBusinessAddresses extends Model
{
    protected $table = 'user_business_addresses';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_business_id',
        'address_label',
        'address_line1',
        'address_line2',
        'rural_id',
        'zip_code',
        'phone',
        'fax',
        'mobile'
    ];

    public function userBusiness()
    {
        return $this->belongsTo('App\UserBusiness', 'user_business_id', 'id');
    }

    public function rurals()
    {
        return $this->belongsTo('App\Rurals', 'rural_id', 'id');
    }

}

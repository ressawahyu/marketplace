<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSnapshots extends Model
{
    protected $table = 'product_snapshots';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'category_id',
        'brand_id',
        'name'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

    public function priceSnapshots()
    {
        return $this->hasMany('App\PriceSnapshots', 'id', 'product_snapshot_id');
    }

}

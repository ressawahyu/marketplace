<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'price';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'min_quality',
        'price'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

}

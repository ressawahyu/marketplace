<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDiscussions extends Model
{
    protected $table = 'product_discussions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'user_id',
        'message',
        'parent_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Values extends Model
{
    protected $table = 'values';
    protected $primaryKey = 'id';

    protected $fillable = [
        'attribute_id',
        'value'
    ];

    public function attributes()
    {
        return $this->hasMany('App\Attributes', 'attribute_id', 'id');
    }

    public function productInstances()
    {
        return $this->hasMany('App\ProductInstances', 'id', 'product_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id',
        'product_instance_id',
        'quality',
        'price'
    ];

    public function productInstances()
    {
        return $this->belongsTo('App\ProductInstances', 'product_instance_id', 'id');
    }

    public function orders()
    {
        return $this->belongsTo('App\Orders', 'order_id', 'id');
    }

}

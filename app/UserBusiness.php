<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBusiness extends Model
{
    protected $table = 'user_business';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'business_name',
        'business_logo'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function userBusinessAddresses()
    {
        return $this->hasMany('App\UserBusinessAddresses', 'id', 'user_business_id');
    }

}

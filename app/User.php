<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function productReviews()
    {
        return $this->hasMany('App\ProductReviews', 'id', 'user_id');
    }

    public function userBusiness()
    {
        return $this->hasMany('App\UserBusiness', 'id', 'user_id');
    }

    public function userWishlist()
    {
        return $this->hasMany('App\UserWishlist', 'id', 'user_id');
    }

    public function userShippingAddresses()
    {
        return $this->hasMany('App\UserShippingAddresses', 'id', 'user_id');
    }

    public function productDiscussions()
    {
        return $this->hasMany('App\ProductDiscussions', 'id', 'user_id');
    }


}

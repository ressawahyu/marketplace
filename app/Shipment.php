<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $table = 'shipment';
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id',
        'package_id',
        'awb_number',
        'date'
    ];

    public function orders()
    {
        return $this->belongsTo('App\Orders', 'order_id', 'id');
    }

    public function courierPackages()
    {
        return $this->belongsTo('App\CourierPackages', 'package_id', 'id');
    }
}

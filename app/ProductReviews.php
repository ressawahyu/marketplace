<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReviews extends Model
{
    protected $table = 'product_reviews';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'user_id',
        'review_message',
        'review_star'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

}

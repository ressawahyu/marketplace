<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAttributes extends Model
{
    protected $table = 'category_attributes';
    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id',
        'attribute_id'
    ];

    public function attributes()
    {
        return $this->hasMany('App\Attributes', 'attribute_id', 'id');
    }

    public function categories()
    {
        return $this->hasMany('App\Categories', 'category_id', 'id');
    }

}

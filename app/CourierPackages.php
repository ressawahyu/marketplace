<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourierPackages extends Model
{
    protected $table = 'courier_packages';
    protected $primaryKey = 'id';

    protected $fillable = [
        'courier_id',
        'packages',
        'price'
    ];

    public function shipment()
    {
        return $this->hasMany('App\Shipment', 'id', 'package_id');
    }

    public function couriers()
    {
        return $this->belongsTo('App\Couriers', 'courier_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'country'
    ];

    public function provinces()
    {
        return $this->hasMany('App\Provinces', 'id', 'country_id');
    }

}

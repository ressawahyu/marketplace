<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rurals extends Model
{
    protected $table = 'rurals';
    protected $primaryKey = 'id';

    protected $fillable = [
        'sub_district_id',
        'rural'
    ];

    public function userBusinessAddresses()
    {
        return $this->hasMany('App\UserBusiness', 'id', 'rural_id');
    }

    public function userShipingAddresses()
    {
        return $this->hasMany('App\UserBusiness', 'id', 'rural_id');
    }

    public function subDistricts()
    {
        return $this->belongsTo('App\SubDistricts', 'sub_district_id', 'id');
    }

}

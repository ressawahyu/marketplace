<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDistricts extends Model
{
    protected $table = 'sub_districts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'district_id',
        'sub_district'
    ];

    public function rurals()
    {
        return $this->hasMany('App\Rurals', 'id', 'sub_district_id');
    }

    public function district()
    {
        return $this->belongsTo('App\Districts', 'district_id', 'id');
    }

}

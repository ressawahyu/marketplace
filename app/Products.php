<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id',
        'brand_id',
        'name'
    ];

    public function productReviews()
    {
        return $this->hasMany('App\ProductReviews', 'id', 'product_id');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImages', 'id', 'product_id');
    }

    public function brands()
    {
        return $this->belongsTo('App\Brands', 'brand_id', 'id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories', 'category_id', 'id');
    }

    public function productDiscussions()
    {
        return $this->hasMany('App\ProductDiscussions', 'id', 'product_id');
    }

    public function price()
    {
        return $this->hasMany('App\Price', 'id', 'product_id');
    }

}

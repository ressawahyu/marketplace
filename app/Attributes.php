<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    protected $table = 'attributes';
    protected $primaryKey = 'id';

    protected $fillable = [
        'attribute'
    ];

    public function categoryAttributes()
    {
        return $this->belongsTo('App\CategoryAttributes', 'id', 'attribute_id');
    }

    public function values()
    {
        return $this->belongsTo('App\Values', 'id', 'attribute_id');
    }

}

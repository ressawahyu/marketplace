<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected $table = 'provinces';
    protected $primaryKey = 'id';

    protected $fillable = [
        'country_id',
        'province'
    ];

    public function districts()
    {
        return $this->hasMany('App\SubDistricts', 'id', 'district_id');
    }

    public function countries()
    {
        return $this->belongsTo('App\Countries', 'country_id', 'id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceSnapshots extends Model
{
    protected $table = 'price_snapshots';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_snapshot_id',
        'min_quality',
        'price'
    ];

    public function productSnapshots()
    {
        return $this->belongsTo('App\ProductSnapshots', 'product_snapshot_id', 'id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Couriers extends Model
{
    protected $table = 'couriers';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'courier',
        'phone',
        'fax',
        'email',
        'website'
    ];

    public function courierPackages()
    {
        return $this->hasMany('App\CourierPackages', 'id', 'courier_id');
    }
}

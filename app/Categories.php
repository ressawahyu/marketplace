<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'country'
    ];

    public function products()
    {
        return $this->hasMany('App\Products', 'id', 'category_id');
    }

    public function categoryAttributes()
    {
        return $this->belongsTo('App\CategoryAttributes', 'id', 'category_id');
    }

}

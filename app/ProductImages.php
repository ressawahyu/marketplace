<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'product_images';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'images'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

}

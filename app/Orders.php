<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';

    protected $fillable = [
        'customer_id',
        'date'
    ];

    public function orderDetail()
    {
        return $this->hasMany('App\OrderDetail', 'id', 'order_id');
    }

    public function shipment()
    {
        return $this->hasMany('App\Shipment', 'id', 'order_id');
    }

}

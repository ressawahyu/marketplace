<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    protected $table = 'districts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'province_id',
        'district'
    ];

    public function subDistricts()
    {
        return $this->hasMany('App\SubDistricts', 'id', 'district_id');
    }

    public function provinces()
    {
        return $this->belongsTo('App\Provinces', 'province_id', 'id');
    }

}

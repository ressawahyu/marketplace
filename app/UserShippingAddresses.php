<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShippingAddresses extends Model
{
    protected $table = 'user_shipping_addresses';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'address_label',
        'address_line1',
        'address_line2',
        'rural_id',
        'zip_code',
        'phone',
        'fax',
        'mobile'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function rurals()
    {
        return $this->belongsTo('App\Rurals', 'rural_id', 'id');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInstances extends Model
{
    protected $table = 'product_instances';
    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'value_id',
        'sku'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

    public function values()
    {
        return $this->belongsTo('App\Values', 'value_id', 'id');
    }

    public function orderDetail()
    {
        return $this->hasMany('App\OrderDetail', 'id', 'product_instance_id');
    }

}

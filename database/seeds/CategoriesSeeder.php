<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('categories')->delete();
        for($i = 1; $i <= 50; $i++)
        {
    		DB::table('categories')->insert([
                'category' => $faker->word,
                'parent_id' => $i,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
    		]);
        }
    }
}

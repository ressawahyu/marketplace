<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('brands')->delete();
        for($i = 1; $i <= 50; $i++)
        {
    		DB::table('brands')->insert([
    			'brand' => $faker->word,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
    		]);
        }
    }
}

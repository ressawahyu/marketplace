<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_snapshots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_snapshot_id')->unsigned();
            $table->integer('min_quality');
            $table->integer('price');
            $table->timestamps();
            $table->foreign('product_snapshot_id')->references('id')->on('product_snapshots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_snapshots');
    }
}

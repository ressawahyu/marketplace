<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBusinessAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_business_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_business_id')->unsigned();
            $table->text('address_label');
            $table->text('address_line1');
            $table->text('address_line2');
            $table->integer('rural_id');
            $table->integer('zip_code')->length(5)->unsigned();
            $table->integer('phone')->length(13)->unsigned();
            $table->integer('fax')->length(13)->unsigned();
            $table->integer('mobile')->length(13)->unsigned();
            $table->timestamps();
            $table->foreign('user_business_id')->references('id')->on('user_business');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_business_addresses');
    }
}
